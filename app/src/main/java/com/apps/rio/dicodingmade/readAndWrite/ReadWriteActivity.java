package com.apps.rio.dicodingmade.readAndWrite;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.apps.rio.dicodingmade.R;
import com.apps.rio.dicodingmade.readAndWrite.Helper.fileHelper;

import java.io.File;
import java.util.ArrayList;

public class ReadWriteActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnNew;
    Button btnOpen;
    Button btnSave;
    EditText editContent;
    EditText editTitle;

    File path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_write);
        btnNew = (Button) findViewById(R.id.button_new);
        btnOpen = (Button) findViewById(R.id.button_open);
        btnSave = (Button) findViewById(R.id.button_save);
        editContent = (EditText) findViewById(R.id.edit_file);
        editTitle = (EditText) findViewById(R.id.edit_title);

        btnNew.setOnClickListener(this);
        btnOpen.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        /* akan secara otomatis memperoleh path dari internal storage aplikasi Anda*/
        path = getFilesDir();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.button_new:
                newFile();
                break;
            case R.id.button_open:
                openFile();
                break;
            case R.id.button_save:
                saveFile();
                break;
        }
    }

    public void newFile() {

        editTitle.setText("");
        editContent.setText("");

        Toast.makeText(this, "Clearing file", Toast.LENGTH_SHORT).show();
    }

//    Method untuk membuka dan menampilkan berkas

    private void loadData(String title) {
        String text = fileHelper.readFromFile(this, title);
        editTitle.setText(title);
        editContent.setText(text);
        Toast.makeText(this, "Loading " + title + " data", Toast.LENGTH_SHORT).show();
    }

    public void openFile() {
        showList();
    }

    private void showList() {
        final ArrayList<String> arrayList = new ArrayList<String>();

        /*untuk menampilkan data internal storage */
        for (String file : path.list()) {

            /*Dengan menggunakan path.list(),
            Anda akan memperoleh semua nama berkas yang ada.
            Tiap berkas yang ditemukan ditambahkan ke dalam obyek arrayList*/
            arrayList.add(file);
        }
        final CharSequence[] items = arrayList.toArray(new CharSequence[arrayList.size()]);

        /*Dengan memanfaatkan AlertDialog pada beberapa baris di atas,
        Anda dapat membuat daftar pilihan berkas sederhana.
        Setelah salah satu berkas pada daftar tersebut di pilih,
        maka aplikasi akan memanggil metode loadData().
        Dengan menggunakan method static pada kelas FileHelper,
        Anda dapat mengambil isi dari file yang dipilih kemudian memasukkannya ke dalam string.*/
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pilih file yang diinginkan");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                loadData(items[item].toString());
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    //    Method untuk menyimpan berkas
    public void saveFile() {
        /*Kita melakukan pemeriksaan if (editTitle.getText().toString().isEmpty()) 
        karena kita membutuhkan nama ketika hendak menyimpan data.
        Dengan menyediakan nama file, konten, dan context,
         Anda dapat langsung menyimpan data dengan memanfaatkan FileHelper Anda.*/
        if (editTitle.getText().toString().isEmpty()) {
            Toast.makeText(this, "Title harus diisi terlebih dahulu", Toast.LENGTH_SHORT).show();
        } else {
            String title = editTitle.getText().toString();
            String text = editContent.getText().toString();
            fileHelper.writeToFile(title, text, this);
            Toast.makeText(this, "Saving " + editTitle.getText().toString() + " file", Toast.LENGTH_SHORT).show();
        }
    }
}
