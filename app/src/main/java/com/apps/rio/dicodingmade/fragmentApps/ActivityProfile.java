package com.apps.rio.dicodingmade.fragmentApps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.apps.rio.dicodingmade.R;

public class ActivityProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }
}
