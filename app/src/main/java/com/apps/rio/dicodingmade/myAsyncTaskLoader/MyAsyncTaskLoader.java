package com.apps.rio.dicodingmade.myAsyncTaskLoader;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by rio senjou on 12/04/2018.
 */

public class MyAsyncTaskLoader extends AsyncTaskLoader<ArrayList<WeatherItems>> {
    private ArrayList<WeatherItems> mData;
    private boolean mHasResult = false;

    private String mKumpulanKota;


    public MyAsyncTaskLoader(Context context, String mKumpulanKota) {
        super(context);
        onContentChanged();
        this.mKumpulanKota = mKumpulanKota;
    }

    /*OnStartLoading() dipanggil setelah proses load berjalan.
    Terdapat percabangan if else yang menuju pada proses forceLoad
    dan deliverResult. */
    @Override
    protected void onStartLoading() {
        if (takeContentChanged())

            /*Metode forceLoad() akan dijalankan ketika data belum tersedia.
             Maka data tersebut akan diambil terlebih dahulu.
             Load data dapat terjadi ketika loader pertama kali dipanggil atau
             ketika terjadi perubahan pada data.*/
            forceLoad();
        else if (mHasResult)

            /*Metode deliverResult() digunakan untuk menampilkan
            result data yang sudah ada.
            Metode ini akan dijalankan juga ketika terjadi reset pada loader.*/
            deliverResult(mData);
    }

    @Override
    public void deliverResult(ArrayList<WeatherItems> data) {
        mData = data;
        mHasResult = true;
        super.deliverResult(data);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mHasResult) {
            onReleaseResources(mData);
            mData = null;
            mHasResult = false;
        }
    }

    private static final String API_KEY = "6311b3980d3e9a3629548030c76ec3a1";

    // Format search kota url JAKARTA = 1642911 ,BANDUNG = 1650357, SEMARANG = 1627896
    // http://api.openweathermap.org/data/2.5/group?id=1642911,1650357,1627896&units=metric&appid=API_KEY


    @Override
    public ArrayList<WeatherItems> loadInBackground() {
        /* akan menjalankan proses pengambilan data secara synchronous*/
        SyncHttpClient client = new SyncHttpClient();

        final ArrayList<WeatherItems> weatherItemses = new ArrayList<>();
        String url = "http://api.openweathermap.org/data/2.5/group?id=" +
                mKumpulanKota+ "&units=metric&appid=" + API_KEY;
        Log.d("url",url);

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();

                /*Meskipun ketika menjalankan client.get()
                kita memasukkan handler AsyncHttpResponseHandler,
                namun prosesnya tetap berjalan secara synchronous*/
                setUseSynchronousMode(true);
                /*Dengan menggunakan setUseSynchronousMode(true),
                kita dapat memanfaatkan handlerloopj yang semula async menjadi
                synchronous sesuai dengan bagaimana client diinisiasi.*/
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    String result = new String(responseBody);
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("list");

                    for (int i = 0 ; i < list.length() ; i++){
                        JSONObject weather = list.getJSONObject(i);
                        WeatherItems weatherItems = new WeatherItems(weather);
                        weatherItemses.add(weatherItems);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                //Jika response gagal maka , do nothing
            }
        });

        return weatherItemses;
    }

    /*Metode onReleaseResources() di atas tidak berisikan kode apapun,
    kita tidak perlu melepaskan memori.
    Berbeda bila kita menggunakan cursor yang membutuhkan proses pelepasan memori.
    Bila memori tidak dibebaskan, maka bisa terjadi kebocoran memori (memory leak).*/
    protected void onReleaseResources(ArrayList<WeatherItems> data) {
        //nothing to do.
    }

}
