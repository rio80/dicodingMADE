package com.apps.rio.dicodingmade.myStackWidget;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Created by rio senjou on 23/05/2018.
 */

public class StackWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new StackRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}
