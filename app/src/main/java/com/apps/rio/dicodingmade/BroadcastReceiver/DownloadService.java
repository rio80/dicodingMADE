package com.apps.rio.dicodingmade.BroadcastReceiver;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class DownloadService extends IntentService {
    public static final String TAG = "DownloadService";
    public DownloadService() {
        super("DownloadService");
    }

           /*Fungsi untuk menjalankan Intent Service dengan process sleep
         dan kemudian mem broadcast sebuah IntentFilter dengan action yang telah ditentukan*/
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG,"Download Service Dijalankan");
        if (intent!=null){
            try {
                Thread.sleep(5000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            Intent notifyFinishIntent = new Intent(smsListenerActivity.ACTION_DOWNLOAD_STATUS);
            sendBroadcast(notifyFinishIntent);
        }
    }


}
