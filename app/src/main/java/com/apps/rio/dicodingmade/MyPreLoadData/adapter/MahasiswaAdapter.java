package com.apps.rio.dicodingmade.MyPreLoadData.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.rio.dicodingmade.MyPreLoadData.model.MahasiswaModel;
import com.apps.rio.dicodingmade.R;

import java.util.ArrayList;

/**
 * Created by rio senjou on 10/05/2018.
 */

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaAdapter.MyViewHolder> {

    private ArrayList<MahasiswaModel> mData = new ArrayList<>();
    private Context context;
    private LayoutInflater mInflater;

    public MahasiswaAdapter(Context context) {
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public void addItem(ArrayList<MahasiswaModel> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mahasiswa_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.textViewNim.setText(mData.get(position).getNim());
        holder.textViewNama.setText(mData.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewNim;
        private TextView textViewNama;

        public MyViewHolder(View itemView) {
            super(itemView);

            textViewNim = (TextView) itemView.findViewById(R.id.txt_nim);
            textViewNama = (TextView) itemView.findViewById(R.id.txt_nama);
        }
    }
}
