package com.apps.rio.dicodingmade.navigasi;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apps.rio.dicodingmade.R;

public class deepNavActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deep_nav);
        Button btnOpenDetail = (Button)findViewById(R.id.btn_open_detail);
        btnOpenDetail.setOnClickListener(this);
        DelayAsync delayAsync =  new DelayAsync();
        delayAsync.execute();
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btn_open_detail){
            Intent detailIntent = new Intent(deepNavActivity.this, detailActivity.class);
            detailIntent.putExtra(detailActivity.EXTRA_TITLE, "Hola, Good News");
            detailIntent.putExtra(detailActivity.EXTRA_MESSAGE, "Now you can learn android in dicoding");
            startActivity(detailIntent);
        }
    }

    public class DelayAsync extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(3000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            showNotification(deepNavActivity.this,"Hai, Apa kabar?",
                    "Apakah kamu punya rencana di hari libur? mari liburan..",
                    110);
        }
    }

    private void showNotification(Context context, String title, String message, int notifId){
        Intent notifDetailIntent = new Intent(deepNavActivity.this, detailActivity.class);
        notifDetailIntent.putExtra(detailActivity.EXTRA_TITLE, title);
        notifDetailIntent.putExtra(detailActivity.EXTRA_MESSAGE, message);
        PendingIntent pendingIntent = TaskStackBuilder.create(this)
                .addParentStack(detailActivity.class)
                .addNextIntent(notifDetailIntent)
                .getPendingIntent(110, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.ic_replay_black_24dp)
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, android.R.color.white))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(alarmSound)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        notificationManagerCompat.notify(notifId, builder.build());
    }
}
