package com.apps.rio.dicodingmade.myIntentApps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.apps.rio.dicodingmade.R;

public class ActivityResult extends AppCompatActivity implements View.OnClickListener {
    private Button btnChoose;
    private RadioGroup rdGrup;
    public static String EXTRA_SEELCTED_VALUE = "extra_selected_value";
    public static int RESULT_CODE = 110;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        btnChoose = (Button) findViewById(R.id.btn_choose);
        rdGrup = (RadioGroup) findViewById(R.id.rgNumber);
        btnChoose.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_choose) {
            if (rdGrup.getCheckedRadioButtonId() != 0) {
                int value = 0;
                switch (rdGrup.getCheckedRadioButtonId()) {
                    case R.id.rb50:
                        value = 50;
                        break;
                    case R.id.rb100:
                        value = 100;
                        break;
                    case R.id.rb150:
                        value = 150;
                        break;
                    case R.id.rb200:
                        value = 200;
                        break;
                }
                Intent resultIntent = new Intent();
                resultIntent.putExtra(EXTRA_SEELCTED_VALUE,value);
                setResult(RESULT_CODE,resultIntent);
                finish();
            }
        }
    }
}
