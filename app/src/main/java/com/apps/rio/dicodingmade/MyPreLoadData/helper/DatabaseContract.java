package com.apps.rio.dicodingmade.MyPreLoadData.helper;

import android.provider.BaseColumns;

/**
 * Created by rio senjou on 10/05/2018.
 */

public class DatabaseContract {
    static String TABLE_NAME = "table_mahasiswa";

    static final class MahasiswaColumns implements BaseColumns {

        // Mahasiswa nama
        static String NAMA = "nama";
        // Mahasiswa nim
        static String NIM = "nim";

    }
}
