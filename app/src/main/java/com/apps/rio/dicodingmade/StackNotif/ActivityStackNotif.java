package com.apps.rio.dicodingmade.StackNotif;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.apps.rio.dicodingmade.R;

import java.util.ArrayList;
import java.util.List;

public class ActivityStackNotif extends AppCompatActivity {

    EditText edtSender, edtMessage;
    Button btnKirim;

    private final static String GROUP_KEY_EMAILS = "group_key_emails";
    private final static int NOTIF_REQUEST_CODE = 200;

    /*Variabel idNotif adalah indeks yang akan kita gunakan untuk mengakses list stackNotif.
     Semua notifikasi yang kita kirimkan akan kita masukkan ke dalam variable stackNotif tersebut.*/

    /*Mengapa perlu disimpan? Hal ini karena kita perlu mengetahui berapa jumlah
    notifikasi yang kita tampilkan. Jumlah tersebut akan kita gunakan sebagai
    parameter untuk menggunakan stack notifikasi.*/
    private int idNotif = 0;
    private int maxNotif = 2;
    List<NotificationItem> stackNotif = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stack_notif);

        edtSender = (EditText) findViewById(R.id.edtSender);
        edtMessage = (EditText) findViewById(R.id.edtMessage);
        btnKirim = (Button) findViewById(R.id.btnKirim);

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sender = edtSender.getText().toString();
                String message = edtMessage.getText().toString();
                if(sender.isEmpty()||message.isEmpty()) {
                    Toast.makeText(ActivityStackNotif.this,"Data harus diisi",Toast.LENGTH_SHORT).show();
                }else{

                    /*Di sini kita membuat obyek baru NotificationItem yang kemudian kita tambahkan
                     pada list stackNotif. Setelah itu kita jalankan metode sendNotif().
                     Proses diakhiri dengan menaikkan variable idnotif dengan menggunakan kode idnotif++.*/

                    NotificationItem notificationItem =new NotificationItem(idNotif,sender,message);
                    stackNotif.add(new NotificationItem(idNotif,sender,message));

                    sendNotif();

                    /*Menaikkan nilai idNotif sangatlah penting karena dia digunakan sebagai indeks
                    untuk mengakses item dari stackNotif yang terakhir kali dimasukkan.*/
                    idNotif++;

                    edtSender.setText("");
                    edtMessage.setText("");
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
                }
            }
        });

    }


    public void sendNotif() {

        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
         /*Kita dapat langsung mendapatkan komponen manager dari notification
        dengan kode di atas. Dan ketika ingin melakukan update
         tinggal panggil manager.notify().*/

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notifications_white_48px);
        Intent intent = new Intent(this, ActivityStackNotif.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, NOTIF_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = null;
        if (idNotif < maxNotif) {

            /*Percabangan Send Notif

            Di dalam metode sendNotif() terdapat percabangan if (idNotif  yang menghitung
            apakah index idNotif sudah melewati nilai dari maxNotif. Jika belum, maka notifikasi
             yang akan ditampilkan adalah notifikasi biasa yang ada pada baris ini:*/
            notification = new NotificationCompat.Builder(this)
                    .setContentTitle("New Email from " + stackNotif.get(idNotif).getSender())
                    .setContentText(stackNotif.get(idNotif).getMessage())
                    .setSmallIcon(R.drawable.ic_mail_white)
                    .setLargeIcon(largeIcon)
                    .setContentIntent(pendingIntent)
                    .setGroup(GROUP_KEY_EMAILS)
                    .setAutoCancel(true)
                    .build();
        } else {

            /*Dan ketika indeks idNotif sudah lebih besar dari nilai maxNotif ,
            maka notifikasinya akan dikelompokkan. Hanya beberapa notifikasi terakhir
            yang akan ditampilkan. Perhatikanlah kode berikut ini:*/
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle()
                    .addLine("New Email from " + stackNotif.get(idNotif).getSender())
                    .addLine("New Email from " + stackNotif.get(idNotif - 1).getSender())

                    /*Kedua addline di atas mengindikasikan bahwa hanya 2 baris yang akan
                     ditampilkan pada kelompok notifikasi. Baris pertama adalah data sender
                      dari stackNotif, dimana nilai indeksnya diperoleh dari idNotif.
                      Sementara itu, baris kedua adalah sender dari stackNotif dimana
                      indexnya diperoleh dari operasi idNotif minus satu.

                    Jika kita ingin menampilkan lebih dari 2 notifikasi,
                    maka tinggal menambahkan metode addline lagi di bawahnya.*/

                    .setBigContentTitle(idNotif + " new emails")
                    .setSummaryText("mail@dicoding");

            /*Yang harus diperhatikan adalah metode setGroup() dan setGroupSummary(true).
            Kedua metode inilah yang akan menjadikan notifikasi menjadi sebuah bundle notification.*/
            notification = new NotificationCompat.Builder(this)
                    .setContentTitle(idNotif + " new emails")
                    .setContentText("mail@dicoding.com")
                    .setSmallIcon(R.drawable.ic_mail_white)
                    .setGroup(GROUP_KEY_EMAILS)
                    .setGroupSummary(true)
                    .setContentIntent(pendingIntent)
                    .setStyle(inboxStyle)
                    .setAutoCancel(true)
                    .build();
        }

        /*merupakan kode untuk menampilkan notifikasi.*/
        manager.notify(idNotif, notification);
    }

    /*Metode ini akan dijalankan ketika ada intent baru (pending intent)
    yang dikirimkan ke dalam activity.
    Dengan menjalankan stackNotif.clear(); dan idNotif=0;
    maka kita menghapus semua data pada list stackNotif dan mengembalikan
    indeks idNotif menjadi 0. Alhasil, semuanya di-reset kembali ketika
    user menekan notifikasi yang ada.*/
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        stackNotif.clear();
        idNotif = 0;
    }
}
