package com.apps.rio.dicodingmade.ServicesApps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apps.rio.dicodingmade.R;

public class ServicesActivity extends AppCompatActivity implements View.OnClickListener{
    private Button startService,startIntentService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        startService = (Button) findViewById(R.id.btn_start_service);
        startIntentService = (Button) findViewById(R.id.btn_start_intent_service);

        startService.setOnClickListener(this);
        startIntentService.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_start_service:
                Intent mStartServiceIntent = new Intent(ServicesActivity.this,OriginService.class);
                startService(mStartServiceIntent);
                break;

            case R.id.btn_start_intent_service:
                Intent mStartIntentService = new Intent(ServicesActivity.this,MyIntentService.class);
                mStartIntentService.putExtra(MyIntentService.EXTRA_DURATION,5000);
                startService(mStartIntentService);
                break;
        }
    }
}
