package com.apps.rio.dicodingmade.SQLite.entity;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.apps.rio.dicodingmade.SQLite.db.DatabaseContract;

import static android.provider.BaseColumns._ID;
import static com.apps.rio.dicodingmade.SQLite.db.DatabaseContract.getColumnInt;
import static com.apps.rio.dicodingmade.SQLite.db.DatabaseContract.getColumnString;

/**
 * Created by rio senjou on 10/05/2018.
 */

public class Note implements Parcelable {

    private int id;
    private String title;
    private String description;
    private String date;

    public Note() {
    }

    /*Penambahan untuk modul Content Provider*/
    public Note(Cursor cursor){
        this.id = getColumnInt(cursor, _ID);
        this.title = getColumnString(cursor, DatabaseContract.NoteColumns.TITLE);
        this.description = getColumnString(cursor, DatabaseContract.NoteColumns.DESCRIPTION);
        this.date = getColumnString(cursor, DatabaseContract.NoteColumns.DATE);
    }
    /*==========================================*/

    protected Note(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        date = in.readString();
    }

    public static final Creator<Note> CREATOR = new Parcelable.Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(date);
    }
}
