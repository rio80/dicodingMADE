package com.apps.rio.dicodingmade.widget;

import java.util.Random;

/**
 * Created by rio senjou on 22/05/2018.
 */

public class NumberGenerator {
    public static int Generate(int max){
        Random random = new Random();
        int randomInt = random.nextInt(max);
        return randomInt;
    }
}
