package com.apps.rio.dicodingmade.myIntentApps;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.apps.rio.dicodingmade.R;

public class myIntentApps extends AppCompatActivity implements View.OnClickListener {
    Button move_activity, move_act_with_data, move_act_with_obj, dial_number, activity_result;
    private TextView tv_result;
    private int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_intent_apps);
        move_activity = (Button) findViewById(R.id.move_activity);
        move_act_with_data = (Button) findViewById(R.id.move_act_with_data);
        move_act_with_obj = (Button) findViewById(R.id.move_act_with_obj);
        dial_number = (Button) findViewById(R.id.dial_number);
        activity_result = (Button) findViewById(R.id.activityResult);
        tv_result = (TextView) findViewById(R.id.tv_result);


        move_activity.setOnClickListener(this);
        move_act_with_data.setOnClickListener(this);
        move_act_with_obj.setOnClickListener(this);
        dial_number.setOnClickListener(this);
        activity_result.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.move_activity:
                i = new Intent(getApplicationContext(), resultIntent.class);
                startActivity(i);
                break;
            case R.id.move_act_with_data:
                i = new Intent(getApplicationContext(), resultIntent.class);
                i.putExtra(resultIntent.EXTRA_NAME, "Rio Firmansyah");
                startActivity(i);
                break;
            case R.id.move_act_with_obj:
                Person mPerson = new Person();
                mPerson.setName("Rio Firmansyah");
                mPerson.setAge(26);
                mPerson.setEmail("rio@gmail.com");
                mPerson.setCity("Bekasi");
                i = new Intent(myIntentApps.this, resultIntent.class);
                i.putExtra(resultIntent.EXTRA_PERSON, mPerson);
                startActivity(i);
                break;
            case R.id.dial_number:
                String numberPhone = "081807277431";
                Intent dial = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + numberPhone));
                startActivity(dial);
                break;
            case R.id.activityResult:
                Intent moveForResultIntent = new Intent(myIntentApps.this, ActivityResult.class);
                startActivityForResult(moveForResultIntent, REQUEST_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode == ActivityResult.RESULT_CODE) {
                int  selectedValue = data.getIntExtra(ActivityResult.EXTRA_SEELCTED_VALUE, 0);

                tv_result.setText("Hasil : " + String.valueOf(selectedValue));
            }
        }
    }
}
