package com.apps.rio.dicodingmade.BroadcastReceiver;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

/**
 * Created by rio senjou on 31/03/2018.
 */

public class PermissionManager {
        public static void check(Activity activity, String permission, int requestCode){
            if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,new String[]{permission},requestCode);
            }
        }
}
