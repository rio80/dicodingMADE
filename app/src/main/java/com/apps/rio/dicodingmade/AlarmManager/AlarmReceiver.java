package com.apps.rio.dicodingmade.AlarmManager;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.apps.rio.dicodingmade.R;

import java.util.Calendar;

/**
 * Created by rio senjou on 02/04/2018.
 */

public class AlarmReceiver extends BroadcastReceiver {
    public static final String TYPE_ONE_TIME = "OneTimeAlarm";
    public static final String TYPE_REPEATING = "RepeatingAlarm";
    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_TYPE = "type";
    private final int NOTIF_ID_ONETIME = 100;
    private final int NOTIF_ID_REPEATING = 101;

    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        /*Sebuah notifikasi sederhana akan tampil pada panel notifikasi pengguna dengan bunyi
        notifikasi umum dan getaran.
        Di sini metode showAlarmNotification(context, title, message, notifId); dijalankan.*/
        String type = intent.getStringExtra(EXTRA_TYPE);
        String message = intent.getStringExtra(EXTRA_MESSAGE);
        String title = type.equalsIgnoreCase(TYPE_ONE_TIME) ? "One Time Alarm" : "Repeating Alarm";
        int notifId = type.equalsIgnoreCase(TYPE_ONE_TIME) ? NOTIF_ID_ONETIME : NOTIF_ID_REPEATING;
        showAlarmNotification(context, title, message, notifId);
    }


    /*Metode dibawah merupakan sebuah method untuk membuat dan menampilkan notifikasi yang kompatibel
    dengan beragam API dari Android. Metode ini memanfaatkan fasilitas NotificationCompat.*/
    private void showAlarmNotification(Context context, String title, String message, int notifId){
        NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.gear)
                .setContentTitle(title)
                .setContentText(message)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(alarmSound);
        notificationManagerCompat.notify(notifId, builder.build());
    }

    public void setOneTimeAlarm(Context context, String type, String date, String time, String message){
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_TYPE, type);

        /*Pada kode di bawah, kita memecah data date dan time
        untuk mengambil nilai tahun, bulan, hari, jam dan menit.*/
        String dateArray[] = date.split("-");
        String timeArray[] = time.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(dateArray[0]));
        calendar.set(Calendar.MONTH, Integer.parseInt(dateArray[1])-1);
        /*Perlu menjadi perhatian mengapa kode di atas, data kita kurangi 1.
        Misal tanggal yang kita masukkan adalah 2016-09-27. Jika kita pecah,
        maka kita akan memperoleh nilai 2016 (tahun), 9 (bulan), dan 27 (hari).

        Masalahnya adalah, nilai bulan ke 9 pada kelas Calendar bukanlah bulan September.
        Ini karena indeksnya dimulai dari 0. Jadi, untuk memperoleh bulan September,
        maka nilai 9 tadi harus kita kurangi 1.*/


        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateArray[2]));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);


        /*Intent yang dibuat akan dieksekusi ketika waktu alarm sama dengan waktu pada sistem Android.
         Di sini komponen PendingIntent  akan diberikan kepada BroadcastReceiver.
         Yang membedakan satu alarm dengan alarm lain adalah pada nilai requestCode.*/
        int requestCode = NOTIF_ID_ONETIME;
        PendingIntent pendingIntent =  PendingIntent.getBroadcast(context, requestCode, intent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        /*Jika kita merubah nilai waktu dan menjalankan ulang alarm dengan requestCode yang sama,
        maka akan merubah yang sudah diset sebelumnya.*/

        /*Maksud dari baris ini alarmManager.set(AlarmManager.RTC_WAKEUP,
        calendar.getTimeInMillis(), pendingIntent); adalah kita memasang alarm yang dibuat dengan tipe RTC_WAKEUP.
        Tipe alarm ini dapat membangunkan peranti (jika dalam posisi sleep) untuk menjalankan obyek PendingIntent.
        Ketika kondisi sesuai, maka akan menjalankan BroadcastReceiver dengan semua proses yang terdapat didalam
        method onReceive()*/
        Toast.makeText(context, "One time alarm set up", Toast.LENGTH_SHORT).show();
    }



    /*Karena tipe alarm yang dikirimkan adalah TYPE_REPEATING,
    maka nilai dari requestCode adalah NOTIF_ID_REPEATING yang bernilai 101.
    Setelah memperoleh requestCode tersebut, maka kita dapat mengetahui Intentnya.
     Dan kemudian kita dapat membatalkan alarmnya.
     Dan itulah esensi membatalkan alarm yang telah dipasang.*/
    public void cancelAlarm(Context context, String type){
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        int requestCode = type.equalsIgnoreCase(TYPE_ONE_TIME) ? NOTIF_ID_ONETIME : NOTIF_ID_REPEATING;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        alarmManager.cancel(pendingIntent);
        Toast.makeText(context, "Repeating alarm dibatalkan", Toast.LENGTH_SHORT).show();
    }



    public void setRepeatingAlarm(Context context, String type, String time, String message){
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_TYPE, type);
        String timeArray[];
         timeArray=  time.split(":");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeArray[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(timeArray[1]));
        calendar.set(Calendar.SECOND, 0);
        int requestCode = NOTIF_ID_ONETIME;
        PendingIntent pendingIntent =  PendingIntent.getBroadcast(context, requestCode, intent, 0);

        /*Disini kita akan mendemonstrasikan sebuah proses yang berulang dengan satuan waktu
        yang ditentukan dengan memanfaatkan fasilitas AlarmManager*/
        /*Baris dibawah akan menjalankan obyek PendingIntent di setiap waktu yang ditentukan
        dalam millissecond dengan interval per hari. Penggunaan metode setInexactRepeating()
        menjadi pilihan yang lebih tepat karena Android akan menjalankan alarm ini
        secara bersamaan dengan alarm lain. Meskipun waktu antara tiap alarm tersebut tidak sama persis.

        Ini penting dilakukan agar baterai device lebih hemat dan tidak cepat habis.
        Ingat! Penggunaan alarm manager yang tidak tepat akan memakan daya baterai yang cukup besar.*/
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        Toast.makeText(context, "Repeating alarm set up", Toast.LENGTH_SHORT).show();
    }

}
