package com.apps.rio.dicodingmade.myAsyncTaskLoader;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rio.dicodingmade.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by rio senjou on 12/04/2018.
 */

public class WeatherAdapter extends BaseAdapter {
    private ArrayList<WeatherItems> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public WeatherAdapter(Context context) {
        this.context = context;
        mInflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /*terjadi proses pengisian item dari
    WeatherItems ke dalam adapter.
    Kemudian metode notifyDataSetChanged() akan dijalankan.*/
    public void setData(ArrayList<WeatherItems> items){
        mData = items;

        /*notifyDataSetChanged() berfungsi untuk mengabari adapter
         bahwa ada data baru yang telah diterima.
         Ketika fungsi ini dijalankan, maka listview yang
         didaftarkan pada adapter akan menampilkan data tersebut.*/
        notifyDataSetChanged();
    }
    public void addItem(final WeatherItems item) {
        mData.add(item);
        notifyDataSetChanged();
    }
    public void clearData(){
        mData.clear();
    }


    @Override
    public int getCount() {
        if (mData == null) return 0;
        return mData.size();
    }

    @Override
    public WeatherItems getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.weather_items, null);
            holder.textViewNamaKota= (TextView)view.findViewById(R.id.textKota);
            holder.textViewTemperature = (TextView)view.findViewById(R.id.textTemp);
            holder.textViewDescription = (TextView)view.findViewById(R.id.textDesc);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.textViewNamaKota.setText(mData.get(i).getNama());
        holder.textViewTemperature.setText(mData.get(i).getTemperature());
        holder.textViewDescription.setText(mData.get(i).getDescription());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context.getApplicationContext(),mData.get(i).getNama().toString(),Toast.LENGTH_SHORT).show();
            }
        });
        return view;


    }

    private static class ViewHolder {
        TextView textViewNamaKota;
        TextView textViewTemperature;
        TextView textViewDescription;
    }
}
