package com.apps.rio.dicodingmade.myIntentApps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.apps.rio.dicodingmade.R;

import org.w3c.dom.Text;

public class resultIntent extends AppCompatActivity {
    public static String EXTRA_NAME = "nama";

    public static String EXTRA_PERSON = "extra_person";
    private TextView tv_object;
    TextView resultIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_intent);
        resultIntent = (TextView)findViewById(R.id.hasil_intent);
        tv_object = (TextView) findViewById(R.id.intent_obj);

        // Untuk class parcelabel, maka getIntent dari class POJO
        Person mPerson = getIntent().getParcelableExtra(EXTRA_PERSON);
        String text = "Name : " + mPerson.getName() + "\nEmail : " + mPerson.getEmail() +
                "\nAge : " + mPerson.getAge() + "\nLocation : " + mPerson.getCity();
        tv_object.setText(text);
    }
}
