package com.apps.rio.dicodingmade.myStackWidget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.apps.rio.dicodingmade.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rio senjou on 23/05/2018.
 */

public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private List<Bitmap> mWidgetItems = new ArrayList<>();
    private Context mContext;
    private int mAppWidgetId;


    public StackRemoteViewsFactory(Context context, Intent intent) {
        mContext = context;
        mAppWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
    }


    /*Proses onCreate di sini dapat digunakan untuk memuat
    semua data yang akan kita gunakan pada widget.
    Proses load di sini tidak boleh lebih dari 20 detik,
    karena jika melebihi dari 20 detik akan terjadi ANR (Application Not Responding).*/
    @Override
    public void onCreate() {
        mWidgetItems.add(BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.gear));
        mWidgetItems.add(BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.athena));
        mWidgetItems.add(BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.confuse));
        mWidgetItems.add(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.gear));
        mWidgetItems.add(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.athena));
    }

    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {

    }


    /*Metode getCount() haruslah mengembalikan
    nilai jumlah isi dari data yang akan kita tampilkan.
    Jika datanya 0, maka tampilan yang ditampilkan akan
    sesuai dengan layout yang kita definisikan pada remoteviews.setEmptyView().*/
    @Override
    public int getCount() {
        return mWidgetItems.size();
    }


    /*Pada metode kita memasang item yang berisikan imageview.
    Kita akan memasang gambar bitmap dengan memanfaatkan remoteviews.
    Kemudian item tersebut akan ditampilkan oleh widget.*/
    @Override
    public RemoteViews getViewAt(int i) {
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);

        rv.setImageViewBitmap(R.id.imageView, mWidgetItems.get(i));
        rv.setTextViewText(R.id.item_text,"ini gambar ke - "+i);

        Bundle extras = new Bundle();
        extras.putInt(ImageBannerWidget.EXTRA_ITEM, i);

        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);

        rv.setOnClickFillInIntent(R.id.imageView, fillInIntent);
        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }


    /*Pada metode getViewTypeCount(), kita perlu mengembalikan nilai yang lebih dari 0.
    Nilai disini mewakili jumlah layout item yang akan kita gunakan pada widget.*/
    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
