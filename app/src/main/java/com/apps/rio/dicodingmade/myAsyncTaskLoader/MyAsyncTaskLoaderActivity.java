package com.apps.rio.dicodingmade.myAsyncTaskLoader;

import android.app.LoaderManager;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.apps.rio.dicodingmade.R;

import java.util.ArrayList;

public class MyAsyncTaskLoaderActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<WeatherItems>>,
AdapterView.OnItemClickListener{
    ListView listView;
    WeatherAdapter adapter;
    EditText editKota;
    Button buttonCari;
    ArrayList<WeatherItems> items = new ArrayList<>();
    static final String EXTRAS_CITY = "EXTRAS_CITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_async_task_loader);

        adapter = new WeatherAdapter(this);
        adapter.notifyDataSetChanged();
        listView = (ListView) findViewById(R.id.listView);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        editKota = (EditText) findViewById(R.id.edit_kota);
        buttonCari = (Button) findViewById(R.id.btn_kota);

        buttonCari.setOnClickListener(myListener);

        String kota = editKota.getText().toString();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRAS_CITY, kota);

        getLoaderManager().initLoader(0, bundle, this);


    }

    @Override
    public Loader<ArrayList<WeatherItems>> onCreateLoader(int i, Bundle bundle) {
        String kumpulanKota = "";
        if (bundle != null) {
            kumpulanKota = bundle.getString(EXTRAS_CITY);
        }

        return new MyAsyncTaskLoader(this, kumpulanKota);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<WeatherItems>> loader, ArrayList<WeatherItems> weatherItems) {
        adapter.setData(weatherItems);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<WeatherItems>> loader) {
        adapter.setData(null);
    }

    View.OnClickListener myListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String kota = editKota.getText().toString();

            if (TextUtils.isEmpty(kota)) return;

            Bundle bundle = new Bundle();
            bundle.putString(EXTRAS_CITY, kota);

             /*Dengan memanggil restartLoader() maka onCreateLoader akan kembali dijalankan.
            Secara otomatis loader baru akan diciptakan. Cara ini lah yang digunakan untuk mengambil
            data kembali dan kemudian menampilkannya di dalam onLoadFinished.*/
            getLoaderManager().restartLoader(0, bundle, MyAsyncTaskLoaderActivity.this);
        }
    };

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(getApplicationContext()," posisi : "+i,Toast.LENGTH_SHORT).show();
    }
}
