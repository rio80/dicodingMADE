package com.apps.rio.dicodingmade;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.apps.rio.dicodingmade.AlarmManager.ActivityAlarm;
import com.apps.rio.dicodingmade.BroadcastReceiver.smsListenerActivity;
import com.apps.rio.dicodingmade.JobScheduler.MainJobScheduler;
import com.apps.rio.dicodingmade.MyPreLoadData.PreloadActivity;
import com.apps.rio.dicodingmade.RecycleViewDanListview.ListPresident;
import com.apps.rio.dicodingmade.SQLite.ActivityNote;
import com.apps.rio.dicodingmade.ServicesApps.ServicesActivity;
import com.apps.rio.dicodingmade.StackNotif.ActivityStackNotif;
import com.apps.rio.dicodingmade.TalkBack.MainTalkBack;
import com.apps.rio.dicodingmade.backgroundNotification.ActivityNotif;
import com.apps.rio.dicodingmade.firebaseCloudMessaging.MainFCM;
import com.apps.rio.dicodingmade.firebaseDispatcher.FirebaseDispatcherActivity;
import com.apps.rio.dicodingmade.fragmentApps.ActivityMainFragment;
import com.apps.rio.dicodingmade.loader.LoaderActivity;
import com.apps.rio.dicodingmade.localization.MainLocalization;
import com.apps.rio.dicodingmade.myActionBar.MyActionBar;
import com.apps.rio.dicodingmade.myAsyncTaskLoader.MyAsyncTaskLoaderActivity;
import com.apps.rio.dicodingmade.myIntentApps.myIntentApps;
import com.apps.rio.dicodingmade.myNavigationDrawer.MyNavigationDrawer;
import com.apps.rio.dicodingmade.myStackWidget.StackWidgetActivity;
import com.apps.rio.dicodingmade.navigasi.deepNavActivity;
import com.apps.rio.dicodingmade.notifDirectReply.DirectReplyActivty;
import com.apps.rio.dicodingmade.readAndWrite.ReadWriteActivity;
import com.apps.rio.dicodingmade.soundPool.SoundPoolActivity;
import com.apps.rio.dicodingmade.threadApps.ActivityThread;
import com.apps.rio.dicodingmade.viewGroups.viewGroups1;
import com.apps.rio.dicodingmade.viewGroups.viewGroups2;
import com.apps.rio.dicodingmade.widget.ActivityWidget;
import com.apps.rio.dicodingmade.widget.BilanganAcakWidget;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    String []judul;
    ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView=(GridView)findViewById(R.id.gridView);
        judul=new String[]{
                "myIntentApp","Fragment","Thread, Handler dan AsyncTask","Services",
                "Broadcast Receiver","Alarm Manager","Job Scheduler","Firebase Dispatcher",
                "Navigasi, Task dan BackStack","Loader","MyAsynTaskLoader","View Groups 1","View Groups 2",
                "List President","My ActionBar","MyNavigationDrawer","Localization","TalkBack",
                "Read and Write file","Sound Pool","SQLite Note Apps","Preload Mahasiswa",
                "Activity Widget","Stack Widget Activity","Background Notification","Direct Reply Notification",
                "Stack Notification","Firebase Cloud Mes"

        };

        arrayAdapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                judul);

        gridView.setAdapter(arrayAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i=null;
               switch (position){
                   case 0:
                       i=new Intent(getApplicationContext(),myIntentApps.class);
                       break;
                   case 1:
                       i=new Intent(getApplicationContext(), ActivityMainFragment.class);
                       break;
                   case 2:
                       i=new Intent(getApplicationContext(), ActivityThread.class);
                       break;
                   case 3:
                       i=new Intent(getApplicationContext(), ServicesActivity.class);
                       break;
                   case 4:
                       i=new Intent(getApplicationContext(), smsListenerActivity.class);
                       break;
                   case 5:
                       i=new Intent(MainActivity.this, ActivityAlarm.class);
                       break;
                   case 6:
                       i=new Intent(MainActivity.this, MainJobScheduler.class);
                       break;
                   case 7:
                       i=new Intent(MainActivity.this, FirebaseDispatcherActivity.class);
                       break;
                   case 8:
                       i=new Intent(MainActivity.this,deepNavActivity.class);
                       break;
                   case 9:
                       i=new Intent(MainActivity.this, LoaderActivity.class);
                       break;
                   case 10:
                       i=new Intent(MainActivity.this, MyAsyncTaskLoaderActivity.class);
                       break;
                   case 11:
                       i=new Intent(MainActivity.this, viewGroups1.class);
                       break;
                   case 12:
                       i=new Intent(MainActivity.this, viewGroups2.class);
                       break;
                   case 13:
                       i=new Intent(MainActivity.this, ListPresident.class);
                       break;
                   case 14:
                       i=new Intent(MainActivity.this, MyActionBar.class);
                       break;
                   case 15:
                       i=new Intent(MainActivity.this, MyNavigationDrawer.class);
                       break;
                   case 16:
                       i=new Intent(MainActivity.this, MainLocalization.class);
                       break;
                   case 17:
                       i=new Intent(MainActivity.this, MainTalkBack.class);
                       break;
                   case 18:
                       i=new Intent(MainActivity.this, ReadWriteActivity.class);
                       break;
                   case 19:
                       i=new Intent(MainActivity.this, SoundPoolActivity.class);
                       break;
                   case 20:
                       i=new Intent(MainActivity.this, ActivityNote.class);
                       break;
                   case 21:
                       i=new Intent(MainActivity.this, PreloadActivity.class);
                       break;
                   case 22:
                       i=new Intent(MainActivity.this, ActivityWidget.class);
                       break;
                   case 23:
                       i=new Intent(MainActivity.this, StackWidgetActivity.class);
                       break;
                   case 24:
                       i=new Intent(MainActivity.this, ActivityNotif.class);
                       break;
                   case 25:
                       i=new Intent(MainActivity.this, DirectReplyActivty.class);
                       break;
                   case 26:
                       i=new Intent(MainActivity.this, ActivityStackNotif.class);
                       break;
                   case 27:
                       i=new Intent(MainActivity.this, MainFCM.class);
                       break;

               }
                startActivity(i);

            }
        });
    }
}
