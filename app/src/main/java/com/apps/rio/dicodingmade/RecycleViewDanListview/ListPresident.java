package com.apps.rio.dicodingmade.RecycleViewDanListview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.apps.rio.dicodingmade.R;
import com.apps.rio.dicodingmade.RecycleViewDanListview.adapter.CardViewPresidentAdapter;
import com.apps.rio.dicodingmade.RecycleViewDanListview.adapter.GridPresidentAdapter;
import com.apps.rio.dicodingmade.RecycleViewDanListview.adapter.ListPresidentAdapter;
import com.apps.rio.dicodingmade.RecycleViewDanListview.classListPresident.President;
import com.apps.rio.dicodingmade.RecycleViewDanListview.classListPresident.PresidentData;

import java.util.ArrayList;

public class ListPresident extends AppCompatActivity {
    RecyclerView rvCategory;
    private ArrayList<President> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_president);

        setActionBarTitle("Mode List");
        rvCategory = (RecyclerView) findViewById(R.id.rv_category);
        rvCategory.setHasFixedSize(true);

        data = new ArrayList<>();
        data.addAll(PresidentData.getListData());

        showRecyclerList();
    }

    private void showRecyclerList() {
        rvCategory.setLayoutManager(new LinearLayoutManager(this));
        ListPresidentAdapter listPresidentAdapter = new ListPresidentAdapter(this);
        listPresidentAdapter.setListPresident(data);
        rvCategory.setAdapter(listPresidentAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String title = null;
        switch (item.getItemId()) {
            case R.id.action_list:
                title = "Mode List";
                showRecyclerList();
                break;

            case R.id.action_grid:
                title = "Mode Grid";
                showRecyclerGrid();
                break;

            case R.id.action_cardview:
                title = "Mode CardView";
                showRecyclerCardView();
                break;
        }
        setActionBarTitle(title);
        return super.onOptionsItemSelected(item);
    }

    private void showRecyclerGrid() {
        rvCategory.setLayoutManager(new GridLayoutManager(this, 2));
        GridPresidentAdapter gridPresidentAdapter = new GridPresidentAdapter(this);
        gridPresidentAdapter.setListPresident(data);
        rvCategory.setAdapter(gridPresidentAdapter);
    }

    private void showRecyclerCardView(){
        rvCategory.setLayoutManager(new LinearLayoutManager(this));
        CardViewPresidentAdapter cardViewPresidentAdapter = new CardViewPresidentAdapter(this);
        cardViewPresidentAdapter.setListPresident(data);
        rvCategory.setAdapter(cardViewPresidentAdapter);
    }
    private void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }
}
